<?php
require_once 'modelo/Conexion.php';
require_once './persistencia/paisesDAO.php';
class paises{
	private $id;
	private $nombre;
	private $region;
	private $conexion;
	private $paisesDAO;
	
	
	
	
	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getNombre()
	{
		return $this->nombre;
	}

	/**
	 * @return string
	 */
	public function getRegion()
	{
		return $this->region;
	}

	/**
	 * @return Conexion
	 */
	public function getConexion()
	{
		return $this->conexion;
	}

	/**
	 * @return paisesDAO
	 */
	public function getPaisesDAO()
	{
		return $this->paisesDAO;
	}

	
	public function __construct($id="", $nombre="",  $region =""){
		$this -> id = $id;
		$this -> nombre = $nombre;
		$this -> region = $region;
		$this -> conexion = new Conexion();
		$this -> paisesDAO = new paisesDAO ($this -> id, $this -> nombre, $this -> region);
		
	}
	
	
	public function traerPaises(){
		$this -> conexion -> abrir();
		$this -> conexion -> ejecutar($this -> paisesDAO -> consultarPaises());
		$paisesT = array();
		while(($registro = $this -> conexion -> extraer()) != null){
			$paises = new paises($registro[0], $registro[1], $registro[2]);
			array_push($paisesT, $paises);
		}
		$this -> conexion -> cerrar();
		return $paisesT;
	}
	
	
	
	
	
	
	
	
	
}
	?>