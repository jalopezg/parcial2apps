<?php
$conexion = mysqli_connect('localhost', 'root', '', 'parcial2');
$idPais = $_GET['idPais'];
// $pedido = new pedido();
// $pedidos = $pedido->consultarPedidos();
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Parcial2</title>
<link
href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet">
	<link rel="stylesheet"
		href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
		<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
		<script
		src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"></script>
		<script
		src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
		</head>
		
		</html>
		
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar Datos de Pais</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>

								<th scope="col" colspan="4" class="text-center table-warning">Datos del Pais</th>

							</tr>
							<tr>
								<th scope="col">Region</th>
								<th scope="col">Codigo</th>
								<th scope="col">Nombre</th>
								<th scope="col">Casos acumulados</th>
								<th scope="col">Muertes acumuladas</th>
								<th scope="col">Casos del ultimo dia reportados</th>
								<th scope="col">Muertes del ultimo dia reportados</th>

							</tr>
						</thead>
						<tbody>	
					<?php
						
						/*
						 * region region a
						 * codigo,nombre country b
						 * report c
						 * */

						$sql = "select DISTINCT a.name, b.id_country, b.name, MAX(c.cumulative_cases),
 MAX(c.cumulative_deaths), c.new_cases, c.new_deaths from region a, country b, report c
WHERE b.id_country='".$idPais."' AND a.id_region = b.id_region_region AND c.date=(SELECT MAX(date) FROM report) AND c.id_country_country = b.id_country";
						$result = mysqli_query($conexion, $sql);

						while ($mostrar = mysqli_fetch_row($result) ) {

							?> 
    <tr>
								<td><?php echo $mostrar[0]; ?></td>
								<td><?php echo $mostrar[1]; ?></td>
								<td><?php echo $mostrar[2]; ?></td>
								<td><?php echo $mostrar[3]; ?></td>
								<td><?php echo $mostrar[4]; ?></td>
								<td><?php echo $mostrar[5]; ?></td>
								<td><?php echo $mostrar[6]; ?></td>
								
							</tr>
    <?php
						}
						
						?>

</tbody>

					</table>


				</div>
			</div>
		</div>
	</div>
	
</html>